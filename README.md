# AgileEngine Test Task for Julian Chennales

This program receives an input HTML file, an element id and a second HTML file. It parses the first HTML file, finds the specified element and analyzes its data. It then parses the second file and attempts to find an element akin to the one in the first file following a set of modular criteria implemented as small finder classes.

    usage: java -jar elementmatcher.jar -b <base_file> -i <base_element_id> -m <modified_file>
    
     -b <arg>   base HTML filename
     -i <arg>   base element id
     -m <arg>   modified HTML filename
    
    All of the arguments are mandatory.

The implementation has three separate sections.

1.    After the basic parameter handling and file reading, the main section implements a simple algorithm in which it presents the base element and its context to a collection of analyzer classes to learn from it and then presents this analysis result and the modified document to a collection of finder classes to find a match. There is no attempt to combine the results of the different finders, just use the best match from all. The priorities and logic should be adjusted to cover rare situations but more information is needed to do this. Ex: is a perfect text match better than an imperfect href/class if the latter is a superset of classes? (ex: "btn btn-success" vs "btn btn-success fancy-decoration"). Do we prioritize elements of the same type? (is an "a" element expected to only match other "a" elements, even though onclick events could be present in others?) 

2.    The analyzer classes. Each analyzer class can inspect the base element and its containing document and return an arbitrary map of what they learn from it. In this example there is a single implementation that obtains the basic HTML information such as the tag name, its attributes and its text. A more advanced analyzer could capture contextual information from siblings, parents and such to improve the search. This example presents a structure so that such a change can be made easily and without affecting the main algorithm or existing analyzers.

3.    The finder classes. Each finder class receives the analysis result and the modified document. Each then implements a strategy for finding an element that matches the base. There is an implementation for the element id which is given the highest priority and only looks for exact matches, an implementation that finds exact href matches and then scores them according to their 'class' similarity and a generic similarity finder which can work on arbitrary bits of the element. This last one is not used directly but instead built upon by a small class that uses it to search using the element's own text. Again, the subclass may be overkill but is aimed at presenting a modular and expandable base upon which changes can be done with ease.

Each finder will provide the best match if any and a score between 0 and 1. The main program will show the best match, the name of the finder that got it and its score. A score of 1.0 can be interpreted as a perfect match of that finder's logic.

Ex: "Best match found by ClassHrefFinder with a score of 1.0: /x/y/z"

This means that the element was found at x/y/z by matching the href exactly and the class with a perfect score.

### Actual output for test cases

    $ java -jar elementmatcher.jar -i make-everything-ok-button -b sample-0-origin.html -m sample-1-evil-gemini.html
    [INFO] 2020-11-10 12:13:55,763 c.a.e.ElementMatcher - Best match found by OwnTextFinder with a score of 1.0: /html/body/div(wrapper)/div(page-wrapper)/div(row)/div(col-lg-8)/div(panel panel-default)/div(panel-body)/a(btn btn-success)
    
    
    $ java -jar elementmatcher.jar -i make-everything-ok-button -b sample-0-origin.html -m sample-2-container-and-clone.html
    [INFO] 2020-11-10 12:14:02,984 c.a.e.ElementMatcher - Best match found by OwnTextFinder with a score of 1.0: /html/body/div(wrapper)/div(page-wrapper)/div(row)/div(col-lg-8)/div(panel panel-default)/div(panel-body)/div(some-container)/a(btn test-link-ok)
    
    
    $ java -jar elementmatcher.jar -i make-everything-ok-button -b sample-0-origin.html -m sample-3-the-escape.html
    [INFO] 2020-11-10 12:14:07,762 c.a.e.ElementMatcher - Best match found by ClassHrefFinder with a score of 1.0: /html/body/div(wrapper)/div(page-wrapper)/div(row)/div(col-lg-8)/div(panel panel-default)/div(panel-footer)/a(btn btn-success)
    
    
    $ java -jar elementmatcher.jar -i make-everything-ok-button -b sample-0-origin.html -m sample-4-the-mash.html
    [INFO] 2020-11-10 12:14:11,498 c.a.e.ElementMatcher - Best match found by ClassHrefFinder with a score of 1.0: /html/body/div(wrapper)/div(page-wrapper)/div(row)/div(col-lg-8)/div(panel panel-default)/div(panel-footer)/a(btn btn-success)
