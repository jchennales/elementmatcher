package com.agileengine.elementmatcher.analyzer;

import java.util.HashMap;
import java.util.Map;

import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class HtmlBasicAnalyzer implements ElementAnalyzer {

	// This basic analyzer will capture the basic attributes and text of an element
	// it does nothing with the containing document (does not care about the element's
	// position within it nor its siblings)

	@Override
	public Map<String, Object> analyzeElement(Element e, Document d) {
		Map<String, Object> res = new HashMap<>();
		
		res.put("tagName", e.tagName());
		res.put("ownText", e.ownText());

		for (Attribute a : e.attributes()) {
			res.put(a.getKey(), a.getValue());
		}
		
		return res;
	}

	@Override
	public String getName() {
		return "HtmlBasicAnalyzer";
	}

}
