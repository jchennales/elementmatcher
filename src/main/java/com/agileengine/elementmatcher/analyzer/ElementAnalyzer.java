package com.agileengine.elementmatcher.analyzer;

import java.util.Map;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public interface ElementAnalyzer {

	// An analyzer inspects and element e and optionally its containing document d and 
	// generates a series of items it "learns" from them.
	//
	// An analyzer is loosely coupled with one or more element finders who can interpret
	// what it learns.
	Map<String, Object> analyzeElement(Element e, Document d);
	
	// name of the analyzer
	String getName();
	
}
