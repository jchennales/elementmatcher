package com.agileengine.elementmatcher.analyzer;

import java.util.ArrayList;
import java.util.List;

public class ElementAnalyzerFactory {

	// This dummy factory might be replaced by Spring beans and autowiring.
	// The idea is not to get into detail for this but just decouple the logic from the available analyzers
	private static List<ElementAnalyzer> elementAnalyzers;
	
	static {
		elementAnalyzers = new ArrayList<>();
		elementAnalyzers.add(new HtmlBasicAnalyzer());
	}
	
	public static List<ElementAnalyzer> getElementAnalyzers() {
		return elementAnalyzers;
	}
	
}
