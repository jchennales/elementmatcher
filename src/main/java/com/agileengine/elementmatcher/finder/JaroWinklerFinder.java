package com.agileengine.elementmatcher.finder;

import java.util.Map;
import java.util.function.Function;

import org.apache.commons.text.similarity.JaroWinklerDistance;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JaroWinklerFinder implements ElementFinder {

	private static Logger LOGGER = LoggerFactory.getLogger(JaroWinklerFinder.class);

	// This finder will look for an element with similar text somewhere inside the base element.
	// It calculates score using JaroWinklerDistance from org.apache.commons.text
	// It is instantiated with functions to pick which piece of the element to compare from the element and the gathered data

	Element closestElement = null;
	double closestElementScore = -1;
	
	JaroWinklerDistance jwd = new JaroWinklerDistance();
	Function<Element, String> elementStringPicker;
	Function<Map<String, Map<String, Object>>, String> analysisStringPicker;
	String name;
	
	JaroWinklerFinder (String name, Function<Element, String> elementStringPicker, Function<Map<String, Map<String, Object>>, String> analysisStringPicker) {
		this.elementStringPicker = elementStringPicker;
		this.analysisStringPicker = analysisStringPicker;
		this.name = name;
	}
	
	@Override
	public void find(Document document, Map<String, Map<String, Object>> elementData) {

		String targetText = analysisStringPicker.apply(elementData);
		
		LOGGER.debug("Looking for '{}'", targetText);

		if (targetText != null) {
			for (Element element : document.body().select("*")) {
				double score = jwd.apply(targetText, elementStringPicker.apply(element));
				LOGGER.debug("Compared against '{}' scored {}", elementStringPicker.apply(element), score);
				if (score > closestElementScore) {
					closestElement = element;
					closestElementScore = score;
				}
			}
		}
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Element getClosestElement() {
		return closestElement;
	}

	@Override
	public double getClosestElementScore() {
		return closestElementScore;
	}

}
