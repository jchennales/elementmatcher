package com.agileengine.elementmatcher.finder;

import java.util.Map;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class IdFinder implements ElementFinder {

	// This basic finder will look for an element with the exact id as the base element.
	// It relies on information gathered by HtmlBasicAnalyzer

	Element foundElement = null;
	
	@Override
	public void find(Document document, Map<String, Map<String, Object>> elementData) {
		Map<String, Object> attributes = elementData.get("HtmlBasicAnalyzer");
		if (attributes != null) {
			String targetId = (String) attributes.get("id");
			if (targetId != null) {
				foundElement = document.getElementById(targetId);
			}
		}
	}

	@Override
	public String getName() {
		return "IdFinder";
	}

	@Override
	public Element getClosestElement() {
		return foundElement;
	}

	@Override
	public double getClosestElementScore() {
		// Only perfect score or nothing
		return foundElement == null? -1 : 1;
	}

}
