package com.agileengine.elementmatcher.finder;

import java.util.Map;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public interface ElementFinder {

	// A finder receives an HTML document and the map of information learnt from the base element 
	// and attempts to find the closest matching element.
	//
	// A finder is loosely coupled with one or more analyzers since it must be able to inspect
	// what the analyzer has learnt from the base element
	//
	// The find method returns the closest matching element and a score.
	// There are no provisions to enforce how the score is calculated but the
	// convention is that a value of 1 means the element fully matches the rules of the finder
	// and values between 0 and 1 mean that one or more of the rules of the finder
	// was only partially matched (as for a non-perfect string match for example)
	// A negative score means no match was found. This could be valid for finders that
	// are not intended to find non-perfect matches (like by id)
	void find(Document document, Map<String, Map<String, Object>> elementData);
	
	// After calling find one can get the element and its score
	Element getClosestElement();
	double getClosestElementScore();
	
	// name of the finder
	String getName();
	
}
