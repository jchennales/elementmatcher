package com.agileengine.elementmatcher.finder;

import java.util.ArrayList;
import java.util.List;

public class ElementFinderFactory {

	// This dummy factory might be replaced by Spring beans and autowiring.
	// The idea is not to get into detail for this but just decouple the logic from the available finders
	private static List<ElementFinder> elementFinders;
	
	static {
		elementFinders = new ArrayList<>();
		elementFinders.add(new IdFinder());
		elementFinders.add(new ClassHrefFinder());
		elementFinders.add(new OwnTextFinder());
	}
	
	public static List<ElementFinder> getElementFinders() {
		return elementFinders;
	}
	
}
