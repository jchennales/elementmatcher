package com.agileengine.elementmatcher.finder;

import java.util.Map;

import org.apache.commons.text.similarity.JaroWinklerDistance;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClassHrefFinder implements ElementFinder {
	
	private static Logger LOGGER = LoggerFactory.getLogger(ClassHrefFinder.class);

	// This finder will look for an element with the exact href as the base element.
	// It will then score the match based on the similarity of the class attribute
	// It relies on information gathered by HtmlBasicAnalyzer

	Element closestElement = null;
	double closestElementScore = -1;
	
	JaroWinklerDistance jwd = new JaroWinklerDistance();
	
	@Override
	public void find(Document document, Map<String, Map<String, Object>> elementData) {
		Map<String, Object> attributes = elementData.get("HtmlBasicAnalyzer");
		if (attributes != null) {
			String targetClass = (String) attributes.get("class");
			if (targetClass == null) targetClass = "";
			String targetHref = (String) attributes.get("href");
			if (targetHref != null) {

				LOGGER.debug("Looking for '{}' and '{}'", targetClass);

				for (Element element : document.body().select("*")) {
					if (targetHref.equals(element.attr("href"))) {
						double score = jwd.apply(targetClass, element.attr("class"));
						LOGGER.debug("Compared against '{}' scored '{}'", element.attr("class"), score);
						if (score > closestElementScore) {
							closestElement = element;
							closestElementScore = score;
						}
					}
				}
			}
		}
	}

	@Override
	public String getName() {
		return "ClassHrefFinder";
	}

	@Override
	public Element getClosestElement() {
		return closestElement;
	}

	@Override
	public double getClosestElementScore() {
		return closestElementScore;
	}

}
