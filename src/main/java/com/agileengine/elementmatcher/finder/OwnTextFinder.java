package com.agileengine.elementmatcher.finder;

import java.util.Map;

public class OwnTextFinder extends JaroWinklerFinder {

	// Build on the JaroWinklerFinder to look for matches of own text

	OwnTextFinder () {
		super("OwnTextFinder", e -> e.ownText(), OwnTextFinder::getOwnTextFromBasicHtmlAnalyzer);
	}
	
	static String getOwnTextFromBasicHtmlAnalyzer(Map<String, Map<String, Object>> elementData) {
		Map<String, Object> attributes = elementData.get("HtmlBasicAnalyzer");
		if (attributes != null) {
			return (String) attributes.get("ownText");
		}
		return null;
	}
	
}
