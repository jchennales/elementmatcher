package com.agileengine.elementmatcher;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.agileengine.elementmatcher.analyzer.ElementAnalyzer;
import com.agileengine.elementmatcher.analyzer.ElementAnalyzerFactory;
import com.agileengine.elementmatcher.finder.ElementFinder;
import com.agileengine.elementmatcher.finder.ElementFinderFactory;

public class ElementMatcher {

	private static Logger LOGGER = LoggerFactory.getLogger(ElementMatcher.class);

	public static void main(String[] args) {

		// Parse command line to get the program parameters
		String baseFileName = null;
		String modifiedFileName = null;
		String baseElementId = null;

		final HelpFormatter helpFormatter = new HelpFormatter();
		final Options options = new Options();
		options.addOption(new Option("b", true, "base HTML filename"));
		options.addOption(new Option("m", true, "modified HTML filename"));
		options.addOption(new Option("i", true, "base element id"));

		CommandLineParser parser = new DefaultParser();
		try {
			CommandLine cmd = parser.parse(options, args);

			baseFileName = cmd.getOptionValue("b");
			modifiedFileName = cmd.getOptionValue("m");
			baseElementId = cmd.getOptionValue("i");

			if (baseFileName == null || modifiedFileName == null || baseElementId == null) {
				LOGGER.error("Invalid parameters");
				helpFormatter.printHelp("ElementMatcher", options);
				System.exit(-1);
			}

		} catch (ParseException e) {
			LOGGER.error("Invalid parameters");
			helpFormatter.printHelp("ElementMatcher", options);
			System.exit(-2);
		}

		// Open files and parse HTML documents, look for base element by id
		Document baseDocument = null;
		Document modifiedDocument = null;

		try {
			File baseFile = new File(baseFileName);
			baseDocument = Jsoup.parse(baseFile, "UTF-8", baseFileName);
		} catch (IOException e) {
			LOGGER.error("Cannot load and parse base file {}", baseFileName);
			System.exit(-3);
		}

		Element baseElement = baseDocument.getElementById(baseElementId);

		if (baseElement == null) {
			LOGGER.error("Cannot find element {} in base file {}", baseElementId);
			System.exit(-4);
		}

		try {
			File modifiedFile = new File(modifiedFileName);
			modifiedDocument = Jsoup.parse(modifiedFile, "UTF-8", modifiedFileName);
		} catch (IOException e) {
			LOGGER.error("Cannot load and parse modified file {}", modifiedFileName);
			System.exit(-5);
		}

		// Present the base element to all available analyzers and collect whatever they learn from it.
		Map<String, Map<String, Object>> elementData = new HashMap<>();

		for (ElementAnalyzer elementAnalyzer : ElementAnalyzerFactory.getElementAnalyzers()) {
			Map<String, Object> res = elementAnalyzer.analyzeElement(baseElement, baseDocument);
			LOGGER.debug("Analyzer {} : {}", elementAnalyzer.getName(), res);
			elementData.put(elementAnalyzer.getName(), res);
		}

		// Call on all available finders to find a matching element in the modified document
		double bestScore = 0;
		Element bestElement = null;
		String bestFinder = null;

		for (ElementFinder elementFinder : ElementFinderFactory.getElementFinders()) {
			elementFinder.find(modifiedDocument, elementData);
			double score = elementFinder.getClosestElementScore();
			if (score > bestScore) {
				// This match is the best one yet
				bestScore = score;
				bestElement = elementFinder.getClosestElement();
				bestFinder = elementFinder.getName();

				// If we find a full score match, we end the search.
				// This assumes finders are listed in desired priority order, something
				// which is left up to the factory
				if (score == 1)
					break;
			}
		}

		// Show the best match (if any)
		if (bestElement != null) {
			List<String> pathElements = new ArrayList<>();
			pathElements.add(frientlyTagName(bestElement));
			for (Element e : bestElement.parents()) {
				pathElements.add(frientlyTagName(e));
			}
			String path = "";
			for (int i = pathElements.size() -1; i >= 0; i--) {
				path += "/" + pathElements.get(i);
			}
			
			LOGGER.info("Best match found by {} with a score of {}: {}", bestFinder, bestScore, path);
		} else {
			LOGGER.info("No match was found for {} on {}", baseElementId, modifiedFileName);
		}

	}

	static String frientlyTagName(Element e) {
		String s = e.tagName();
		if (!e.id().isEmpty()) {
			s += "(" + e.id() + ")";
		}
		else if (!e.className().isEmpty()) {
			s += "(" + e.className() + ")";
		}
		return s;
	}
	
}
