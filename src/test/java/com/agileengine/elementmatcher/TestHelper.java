package com.agileengine.elementmatcher;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.Assertions;

import com.agileengine.elementmatcher.analyzer.HtmlBasicAnalyzer;

public class TestHelper {

	public Document xylophoneDocument;
	public Document violinDocument;
	public Document carDocument;
	public Element xylophoneElement;
	public Element violinElement;
	public Element carElement;
	public Map<String, Map<String, Object>> xylophoneElementData;
	public Map<String, Map<String, Object>> violinElementData;
	public Map<String, Map<String, Object>> carElementData;

	public TestHelper() {
	
        ClassLoader classLoader = getClass().getClassLoader();
        
        File xylophoneFile = new File(classLoader.getResource("xylophone.html").getFile());
        File violinFile = new File(classLoader.getResource("violin.html").getFile());
        File carFile = new File(classLoader.getResource("car.html").getFile());
        
		try {
			xylophoneDocument = Jsoup.parse(xylophoneFile, "UTF-8", "");
			violinDocument = Jsoup.parse(violinFile, "UTF-8", "");
			carDocument = Jsoup.parse(carFile, "UTF-8", "");
		} catch (IOException e) {
			Assertions.fail("Cannot load and parse test resources.");
		}
        
		xylophoneElement = xylophoneDocument.getElementById("xylophone");
		HtmlBasicAnalyzer xylophoneAnalyzer = new HtmlBasicAnalyzer();
		xylophoneElementData = new HashMap<>();
		xylophoneElementData.put(xylophoneAnalyzer.getName(), xylophoneAnalyzer.analyzeElement(xylophoneElement, null));

		violinElement = violinDocument.getElementById("violin");
		HtmlBasicAnalyzer violinAnalyzer = new HtmlBasicAnalyzer();
		violinElementData = new HashMap<>();
		violinElementData.put(violinAnalyzer.getName(), violinAnalyzer.analyzeElement(violinElement, null));

		carElement = carDocument.getElementById("car");
		HtmlBasicAnalyzer carAnalyzer = new HtmlBasicAnalyzer();
		carElementData = new HashMap<>();
		carElementData.put(carAnalyzer.getName(), carAnalyzer.analyzeElement(carElement, null));
		
	}
	
}
