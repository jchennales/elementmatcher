package com.agileengine.elementmatcher.analyzer;

import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.agileengine.elementmatcher.TestHelper;

class HtmlBasicAnalyzerTest {

	private static TestHelper th;

	@BeforeAll
	static void setup() {

		th = new TestHelper();

	}

	@Test
	void analyzeTest() {
		
		HtmlBasicAnalyzer xylophoneAnalyzer = new HtmlBasicAnalyzer();
		
		Map<String, Object> basicData = xylophoneAnalyzer.analyzeElement(th.xylophoneElement, th.xylophoneDocument);
		
		Assertions.assertEquals(th.xylophoneElement.id(), basicData.get("id"));
		Assertions.assertEquals(th.xylophoneElement.ownText(), basicData.get("ownText"));
		Assertions.assertEquals(th.xylophoneElement.className(), basicData.get("class"));
		Assertions.assertEquals(th.xylophoneElement.tag().getName(), basicData.get("tagName"));
		
	}

}
