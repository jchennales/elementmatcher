package com.agileengine.elementmatcher.finder;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.agileengine.elementmatcher.TestHelper;

class ClassHrefFinderTest {

	private static TestHelper th;

	@BeforeAll
	static void setup() {

		th = new TestHelper();

	}

	@Test
	void findExact() {

		ClassHrefFinder finder = new ClassHrefFinder();

		finder.find(th.xylophoneDocument, th.xylophoneElementData);

		Assertions.assertEquals(1L, finder.getClosestElementScore());

	}

	@Test
	void findInexact() {

		ClassHrefFinder finder = new ClassHrefFinder();

		finder.find(th.xylophoneDocument, th.violinElementData);

		Assertions.assertTrue(finder.getClosestElementScore() > 0 && finder.getClosestElementScore() < 1);

	}

	@Test
	void findNonExisting() {

		ClassHrefFinder finder = new ClassHrefFinder();

		finder.find(th.xylophoneDocument, th.carElementData);

		Assertions.assertEquals(-1L, finder.getClosestElementScore());

	}

}
