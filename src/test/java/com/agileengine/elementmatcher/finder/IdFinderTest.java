package com.agileengine.elementmatcher.finder;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.agileengine.elementmatcher.TestHelper;

class IdFinderTest {

	private static TestHelper th;

	@BeforeAll
	static void setup() {

		th = new TestHelper();

	}

	@Test
	void findExistingId() {

		IdFinder finder = new IdFinder();

		finder.find(th.xylophoneDocument, th.xylophoneElementData);

		Assertions.assertEquals(1L, finder.getClosestElementScore());

	}

	@Test
	void findNonExistingId() {

		IdFinder finder = new IdFinder();

		finder.find(th.xylophoneDocument, th.violinElementData);

		Assertions.assertEquals(-1L, finder.getClosestElementScore());

	}

}
