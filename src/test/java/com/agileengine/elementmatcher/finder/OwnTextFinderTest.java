package com.agileengine.elementmatcher.finder;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.agileengine.elementmatcher.TestHelper;

class OwnTextFinderTest {

	private static TestHelper th;

	@BeforeAll
	static void setup() {

		th = new TestHelper();

	}

	@Test
	void findExact() {

		OwnTextFinder finder = new OwnTextFinder();

		finder.find(th.xylophoneDocument, th.xylophoneElementData);

		Assertions.assertEquals(1L, finder.getClosestElementScore());

	}

	@Test
	void findInexact() {

		OwnTextFinder finder = new OwnTextFinder();

		finder.find(th.xylophoneDocument, th.violinElementData);

		Assertions.assertTrue(finder.getClosestElementScore() > 0.5 && finder.getClosestElementScore() < 1);

	}

	@Test
	void findNonExisting() {

		OwnTextFinder finder = new OwnTextFinder();

		finder.find(th.xylophoneDocument, th.carElementData);

		// own text matching will almost always find *something* no matter how bad a match.
		Assertions.assertTrue(finder.getClosestElementScore() > 0 && finder.getClosestElementScore() < 0.5);

	}

}
